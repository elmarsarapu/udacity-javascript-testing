describe('Contact', () => {
  it('should new contact to be equal `Jonathan Bergson`', () => {
    const name = 'Jonathan Bergson'
    const contact = new Contact(name);
    expect(contact).toEqual({ name: name });
  });

  it('should new contact to be equal `Larissa da Silva`', () => {
    const name = 'Larissa da Silva';
    const contact = new Contact(name);
    expect(contact).toEqual({ name: name });
  });
});